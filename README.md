## Objectives

1. Students will practice some basic Ruby, including variables and strings

## Resources

* [Ruby Doc's Intro to Ruby](http://ruby-doc.org/docs/ruby-doc-bundle/Tutorial/index.html)
* [Ruby Strings](http://ruby-doc.org/core-2.1.2/String.html)

<p class='util--hide'>View <a href='https://learn.co/lessons/kwk-teachers-guide-intro-to-ruby'>KWK Teachers Guide Intro to Ruby</a> on Learn.co and start learning to code for free.</p>
